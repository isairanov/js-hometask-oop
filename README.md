# js-hometask-oop

## Ответьте на вопросы

### Откуда в объектах методы `toString` и `valueOf`, хотя вы их не добавляли?
> Ответ: Все объекты в js — это экземпляры класса Object, у которого есть методы toString, valueOf, hasOwnProperty. Поэтому мы их можем вызвать из любого объекта

### Чем отличаются `__proto__` и `prototype`? 
> Ответ: `__proto__` - это свойство объекта, которое указывает на родительский класс. Если свойство не найдено в самом объекта, то оно будет искаться по ссылке в поле `__proto__` по цепочке портотипов.<br>
`prototype` - это свойство конструктора, в который можно присвоить объект 

### Что такое `super` в классах?
> Ответ: ключевого слова `super` даёт доступ к методам родителя. Например, super() вызывает конструктор родителя 

### Что такое статический метод класса, как его создать? 
> Ответ: метод, который не привязан к конкретному объекту и может вызываться непосредственно из класса, например Number.isInteger() выглядит как статический метод (но является ли им?). Создается с помощью ключевого слова `static`
```js
class Console {
  static print(message) {
    console.log(message);
  }
}
Console.print('yo');
```


## Выполните задания

* Установите зависимости `npm install`;
* Допишите функции в `tasks.js`;
* Проверяте себя при помощи тестов `npm run test`;
* Создайте Merge Request с решением.
