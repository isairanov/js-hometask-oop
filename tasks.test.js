const { Leopard, Cat, changeMap, Tiger, Dog, Wolf, Frog, Reptile, Turtle } = require('./tasks');


describe('Cat and Leopard', () => {
  test('у кота есть методы sayMeow и getPet', () => {
    const cat = new Cat();
    expect(cat.sayMeow).toBeDefined();
    expect(cat.getPet).toBeDefined();
  });
  test('у леопарда есть методы sayMeow и getPet', () => {
    const leopard = new Leopard();
    expect(leopard.sayMeow).toBeDefined();
    expect(leopard.getPet).toBeDefined();
  });
  test('леопард наследник от Cat', () => {
    const leopard = new Leopard();
    expect(leopard instanceof Cat).toBe(true);
  });
  test('у кота методы sayMeow и getPet возвращают правильные строки', () => {
    const cat = new Cat();
    expect(cat.sayMeow()).toBe('Meow!   Meow!   Meow!');
    expect(cat.getPet()).toBe('Purrrrrr');
  });
  test('у леопарда метод sayMeow возвращает правильную строчку', () => {
    const leopard = new Leopard();
    expect(leopard.sayMeow()).toBe('Roar!');
  });
  test('у леопарда метод getPet возвращает правильную строчку', () => {
    const leopard = new Leopard();
    expect(leopard.getPet()).toBe('Purrrrrr');
  });
  test('у леопарда метод getPet делает его счастливым', () => {
    const leopard = new Leopard();
    leopard.getPet();
    expect(leopard.mood).toBe('счастлив');
  });
  test('у леопарда счастливое настроение появляется только после вызова getPet', () => {
    const leopard = new Leopard();
    expect(leopard.mood).not.toBe('счастлив');
    leopard.getPet();
    expect(leopard.mood).toBe('счастлив');
  });
  test('у леопардов счастливое настроение устанавливается независимо', () => {
    const leopard = new Leopard();
    const leopard2 = new Leopard();
    expect(leopard.mood).not.toBe('счастлив');
    expect(leopard2.mood).not.toBe('счастлив');
    leopard.getPet();
    expect(leopard.mood).toBe('счастлив');
    expect(leopard2.mood).not.toBe('счастлив');
  });
});

describe('changeMap', () => {
  const map = Array.prototype.map;
  test('до вызова функции changeMap map работает нормально', () => {
    expect([].map(item => item)).toEqual([]);
    expect([1,2].map(item => item + 1)).toEqual([2,3]);
  });
  test('после вызова функции changeMap map возвращает строку', () => {
    changeMap();
    expect([].map()).toEqual('метод устал и не хочет работать!');
    expect([1,2].map(item => item + 1)).toEqual('метод устал и не хочет работать!');
  });
  afterEach(() => {
    Array.prototype.map = map;
  })
});

describe('Tiger', () => {
  test('создает объект тигра', () => {
    const tiger = new Tiger('Tom', 'amur');
    expect(typeof tiger).toBe('object');
    expect(tiger instanceof Tiger).toBe(true);
  });
  test('у тигра есть имя и тип', () => {
    const tiger = new Tiger('Tom', 'amur');
    expect(tiger.name).toBe('Tom');
    expect(tiger.type).toBe('amur');
  });
  test('у тигра есть метод подсчета полосок', () => {
    const tiger = new Tiger('Tom', 'amur');
    expect(tiger.countStrips).toBeDefined();
  });
  test('метод подсчета полосок считает их', () => {
    const tiger = new Tiger('Tom', 'amur');
    expect(tiger.strips).not.toBeDefined();
    tiger.countStrips();
    expect(tiger.strips).toBe(1);
    tiger.countStrips();
    expect(tiger.strips).toBe(2);
  });
});


describe('Wolf', () => {
  test('при создании волка у него есть ранг и флаг что волк полярный', () => {
    const wolf = new Wolf('alfa', false);
    expect(wolf.rank).toBe('alfa');
    expect(wolf.isArcticWolf).toBe(false);
  });
  test('при создании волка у него есть метод охоты', () => {
    const wolf = new Wolf('alfa', false);
    expect(wolf.hunt).toBeDefined();
  });
  test('при создании волка у него есть методы от собаки', () => {
    const wolf = new Wolf('alfa', false);
    expect(wolf.sayWoof).toBeDefined();
    expect(wolf.getFood).toBeDefined();
  });
  test('методы собаки принадлежат не волку', () => {
    const wolf = new Wolf('alfa', false);
    expect(wolf.hasOwnProperty('sayWoof')).toBe(false);
    expect(wolf.hasOwnProperty('getFood')).toBe(false);
  });
  test('при создании собаки у нее есть методы', () => {
    const dog = new Dog();
    expect(dog.sayWoof).toBeDefined();
    expect(dog.getFood).toBeDefined();
  });
  test('метод охоты меняет сытость волка', () => {
    const wolf = new Wolf('alfa', false);
    expect(wolf.fullness).not.toBeDefined();
    wolf.hunt();
    expect(wolf.fullness).toBe(25);
    wolf.hunt();
    expect(wolf.fullness).toBe(50);
  });
  test('метод getFood меняет сытость волка', () => {
    const wolf = new Wolf('alfa', false);
    expect(wolf.fullness).not.toBeDefined();
    wolf.getFood();
    expect(wolf.fullness).toBe(25);
    wolf.getFood();
    expect(wolf.fullness).toBe(50);
  });
  test('методы кормления работают независимо', () => {
    const wolf = new Wolf('alfa', false);
    const wolf2 = new Wolf('omega', true);
    expect(wolf.fullness).not.toBeDefined();
    expect(wolf2.fullness).not.toBeDefined();
    wolf.getFood();
    expect(wolf.fullness).toBe(25);
    expect(wolf2.fullness).not.toBeDefined();
    wolf.getFood();
    expect(wolf.fullness).toBe(50);
    expect(wolf2.fullness).not.toBeDefined();
  });

});

describe('Frog', () => {
  beforeEach(() => {
    Frog.femaleCount = 0;
    Frog.maleCount = 0;
  });

  test('у лягушки должен быть пол', () => {
    const frog = new Frog('female');
    expect(frog.sex).toBe('female');
  });
  test('есть статическое поле-каунтер', () => {
    new Frog('female');
    expect(Frog.femaleCount).toBe(1);
  });
  test('поле второго пола равно 0', () => {
    new Frog('female');
    expect(Frog.maleCount).toBe(0);
  });
  test('есть статический метод подсчета соотношения полов', () => {
    new Frog('female');
    new Frog('male');
    expect(Frog.getGenderRatio).toBeDefined();
    expect(Frog.getGenderRatio()).toBe(1);
  });
  test('метод updateGender превращает лишнюю самку в самца', () => {
    const frog = new Frog('female');
    for (let i = 0; i < 5; i++) {
      new Frog('female');
    }
    for (let i = 0; i < 3; i++) {
      new Frog('male');
    }
    expect(Frog.getGenderRatio()).toBe(0.5);
    frog.updateGender();
    expect(frog.sex).toBe('male');
    expect(Frog.getGenderRatio()).toBe(0.8);
    expect(Frog.maleCount).toBe(4);
    expect(Frog.femaleCount).toBe(5);
  });
  test('метод updateGender не трогает самца, при большом количестве самок', () => {
    const frog = new Frog('male');
    for (let i = 0; i < 6; i++) {
      new Frog('female');
    }
    for (let i = 0; i < 2; i++) {
      new Frog('male');
    }
    expect(Frog.getGenderRatio()).toBe(0.5);
    frog.updateGender();
    expect(frog.sex).toBe('male');
    expect(Frog.getGenderRatio()).toBe(0.5);
    expect(Frog.maleCount).toBe(3);
    expect(Frog.femaleCount).toBe(6);
  });
  test('метод updateGender превращает лишнего самца в самку', () => {
    const frog = new Frog('male');
    for (let i = 0; i < 5; i++) {
      new Frog('male');
    }
    for (let i = 0; i < 3; i++) {
      new Frog('female');
    }
    expect(Frog.getGenderRatio()).toBe(2);
    frog.updateGender();
    expect(frog.sex).toBe('female');
    expect(Frog.getGenderRatio()).toBe(1.25);
    expect(Frog.maleCount).toBe(5);
    expect(Frog.femaleCount).toBe(4);
  });
  test('метод updateGender не трогает самку, при большом количестве самцов', () => {
    const frog = new Frog('female');
    for (let i = 0; i < 6; i++) {
      new Frog('male');
    }
    for (let i = 0; i < 2; i++) {
      new Frog('female');
    }
    expect(Frog.getGenderRatio()).toBe(2);
    frog.updateGender();
    expect(frog.sex).toBe('female');
    expect(Frog.getGenderRatio()).toBe(2);
    expect(Frog.maleCount).toBe(6);
    expect(Frog.femaleCount).toBe(3);
  });
  test('метод updateGender не трогает никого, если соотношение нормальное', () => {
    const femaleFrog = new Frog('female');
    const maleFrog = new Frog('male');
    expect(Frog.getGenderRatio()).toBe(1);
    femaleFrog.updateGender();
    expect(femaleFrog.sex).toBe('female');
    maleFrog.updateGender();
    expect(maleFrog.sex).toBe('male');
  });
});

describe('Turtle', () => {
  test('черепаха наследник рептилий', () => {
    const turtle = new Turtle();
    expect(turtle instanceof Reptile).toBe(true);
  });
  test('черепаха умеет бегать и двигаться', () => {
    const turtle = new Turtle();
    expect(turtle.move).toBeDefined();
    expect(turtle.run).toBeDefined();
  });
  test('рептилии тоже умеют бегать и двигаться', () => {
    const reptile = new Reptile();
    expect(reptile.move).toBeDefined();
    expect(reptile.run).toBeDefined();
  });
  test('при беге скорость сухопутной черепахи равна 2', () => {
    const turtle = new Turtle();
    turtle.run();
    expect(turtle.speed).toBe(2);
  });
  test('при движении скорость сухопутной черепахи равна 1', () => {
    const turtle = new Turtle();
    turtle.move();
    expect(turtle.speed).toBe(1);
  });
  test('при беге скорость морской черепахи равна 1', () => {
    const turtle = new Turtle(true);
    turtle.run();
    expect(turtle.speed).toBe(1);
  });
  test('при движении скорость морской черепахи равна 1', () => {
    const turtle = new Turtle(true);
    turtle.move();
    expect(turtle.speed).toBe(1);
  });
});
